<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
    <script type = "text/javascript">          
      // Below Function Executes On Form Submit
      function ValidationEvent() {
        // Storing Field Values In Variables
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        // Regular Expression For Email
        var emailReg = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
        // Conditions
        if (email != '' && password != '') {
          if (email.match(emailReg)) {
            if (password.length >= 8 && password.length <= 15) {
              return true;
            } 
            else {
              alert("The password must be between 8 and 15 characters long!");
              return false;
            }
          } 
          else {
            alert("Please enter a valid email address!");
            return false;
          }
        } 
        else {
          alert("All fields are required!");
          return false;
        }
      }
    </script>
  </head>
  <body bgcolor = "#DEB887">
    <form method="post" action="http://localhost:8080/zomato/user" 
          onsubmit="return ValidationEvent()">
    <table border="1" width="30%" cellpadding="3" align="center">
     <thead>
       <tr>
         <th colspan="2">
           <center>Welcome! Please Login Here! </center>
         </th>
       </tr>
     </thead>
     <tbody>
       <tr>
         <td>Email ID</td>
         <td>
           <input id="email" type="email" name="email" 
                  onblur="validateEmail(this.value)"
                  placeholder="Enter a valid email address" required/>
         </td>
       </tr>
       <tr>
         <td>Password</td>
         <td><input id="password" type="password" name="password"
                    placeholder="Enter Password" required/>
         </td>
       </tr>
       <tr>
         <td><input type="reset" value="Reset" /></td>
         <td><input type="submit" value="login" name="submit" /></td>
       </tr>
       <tr>
         <td>Forgot Password?</td>
         <td><a href="ForgotPassword.jsp"> Click Here <a/></td>
       </tr>
     </tbody>
   </table>
     <br>
     <center>
     <img src="media/online-food-ordering.png" width="1250" height="500">
     </center>
     </form>
  </body>
</html>
