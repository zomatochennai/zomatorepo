<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Access Denied!!!</title>
  </head>
  <style type "text/css">
    .blink {
      -webkit-animation: blink .75s linear infinite;
      -moz-animation: blink .75s linear infinite;
      -ms-animation: blink .75s linear infinite;
      -o-animation: blink .75s linear infinite;
       animation: blink .75s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    div.ex {
      text-align: center width:300px;
      padding: 20px;
      border: 0px white;
      margin: 5px
    }
  </style>
  <body bgcolor = "#F08080">
    <center><h1><b><p class="tab blink">Access denied!!!!</p></b></h1></center>
    <div class="ex">
      <center>
      <img border="0" alt="STOP!" src="media/danger-restricted-area-sign.gif" 
           width="400" height="300">
      <br>
      <h2>You are trying to acess the part of site you have no access for!</h2>
      <h3>You can go back <a href = "Welcome.jsp">here</a> and start again.<h3>
      <br>
      <img border="0" alt="Not Allowed!" src="media/logo-accessdenied.png" 
           width="150" height="150">
      <img border="0" alt="Not Allowed!" src="media/StopIT.png" width="150"
           height="150">
      </center>      
    </div>
  </body>
</html>
