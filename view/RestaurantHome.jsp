<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
%>
<%@ include file="scripts/CookieManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restaurant Home Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <h1>Restaurant Home Page</h1>
      <form action="http://localhost:8080/zomato/user" method="post">
        <table style="with: 5%" align="right">
          <tr>
            <td>
              <input type="submit" value="logout" name="submit" />
            </td>
          </tr>
        </table>
      </form>
    <div id="top">
      Hello! ${user.firstName}, Welcome to your own online Food shop! 
    </div>
    <div class="ex">
      <caption>
        <h2>Please choose an Action to perform:</h2>
      </caption>
      <table style="with: 50%">
        <form action="http://localhost:8080/zomato/restaurant" method="post">
          <table style="with: 20%">
            <tr>
              <th>
                <td>
                  <a href="CreateNewRestaurant.jsp"> 
                    <input type="button" value="add restaurant" >
                  </a>
                </td>
                <td>
                  <a href="http://localhost:8080/zomato/menuitem?submit=get
                           menuitems&targetPage=view/UpdateMenuItem.jsp"> 
                    <input type="button" value="add or update menuitems" >
                  </a>
                </td>
                <td>
                  <a href="http://localhost:8080/zomato/order?submit=get
                           trackings&targetPage=view/ManageTracking.jsp"> 
                    <input type="button" value="manage tracking" >
                  </a>
                </td>
              </th>
            </tr>
        </table>
      </div>
  </body>
</html>
