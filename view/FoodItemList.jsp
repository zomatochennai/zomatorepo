<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="e" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restaurant Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
   <h1>Restaurant Order Page</h1>
     <form action="http://localhost:8080/zomato/user" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="logout" name="submit" /></td>
         </tr>
       </table>
    </form>
    <div class="ex">
      <form action="http://localhost:8080/zomato/menuItem" method="get">
        <table style="with: 50%">
          <caption><h2>Restaurant Details</h2></caption>
           <tr>
             <th>${restaurant.name}</th>
             <th>${restaurant.description}</th>
             <th>${restaurant.startingTime}</th>
             <th>${restaurant.closingTime}</th>
           </tr>
         </table>
         <br>
         <table style="with: 20%">
           <tr>
             <th>${catogory.name}</th>
           </tr>
         </table>
         <table style="with: 80%">
           <h3>List of ${catogery.name} Options Available 
               Options at ${restaurant.name}
           </h3>
           <tr>
             <th>Food Name</th>
             <th>Food Description</th>
             <th>Food Price</th>
             <th>Order Food</th>
           </tr>
           <tr>
             <e:forEach var="menuItem" items="${menuItems}">
               <td><e:out value="${menuItem.name}" /></td>
               <td><e:out value="${menuItem.description}" /></td>
               <td><e:out value="${menuItem.price}" /></td>
               <td><a href="http://localhost:8080/zomato/menuItem
                            ?id=${menuItem.id}&submit=order menuItem"> 
                     "Order from  
                   <e:out value="${classification.name}" /> !"
                   </a> 
               </td>
             </e:forEach>
           </tr>
         </table>
      </form>
    </div>
  </body>
</html>
