<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Page not Found</title>
  </head>
  <style>
    div.ex {
      text-align: center width:300px;
      padding: 20px;
      border: 0px white;
      margin: 5px
    }
  </style>
  <body bgcolor = "#6495ED">
    <center><b><h1>404! Page not found!</h1></b></center>
    <div class="ex">
      <center>
      <img border="0" alt="Grim Reaper!" src="media/four-oh-four_08.png" width="500" 
           height="300">
      <br>
      <h2><i>
        The page you are looking for has been moved or does not exist!
      </i></h2>
      <br>
      <a href = "Welcome.jsp">
      <img border="0" alt="Go to Mainland!" src="media/Error-404-Pages.jpg" 
           width="750" height="500">
      </a>
      </center>      
    </div>
  </body>
</html>
