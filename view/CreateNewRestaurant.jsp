<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
      Restaurant Creation Page
    </title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body bgcolor="#F5F5DC">
    <center><h1></i>New Restaurant Form</i></h1></center>
    <div class="ex">
      <form id="newrestaurant" action="http://localhost:8080/zomato/restaurant" 
            method="post">
        <table style="with: 60%">
          <tr>
            <td>Restaurant Name *</td>
            <td><input type="text" name="name" 
                       placeholder="Please Enter a value for this field"
                       required/>
            </td>
          </tr>
          <tr>
            <td>Restaurant Classification *</td>
            <td><input type="text" name="restaurantClassification"
                       placeholder="Please Enter a value for this field" 
                       required/>
            </td>
          </tr>
          <tr>
            <td>Please enter Restaurant Description *</td>
            <td><input type="text" name="description" 
                       placeholder="Please Enter a value for this field" 
                       required/>
            </td>
          </tr>
          <tr>
            <td>Starting Time *</td>
            <td><input type="text" name="startingTime"
                       placeholder="Please Enter a value for this field" 
                       required/>
            </td>
          </tr>
          <tr>
            <i> Please enter time in 24 Hrs format as HH:MM only</i>
          </tr>
          <tr>
            <td>Closing Time *</td>
            <td><input type="text" name="closingTime" 
                       placeholder="Please Enter a value for this field" 
                       required />
            </td>
          </tr>
          <tr>
            <i> Please enter time in 24 Hrs format as HH:MM only</i>
          </tr>
          <tr>
            <td>Home Delivery </td>
            <td><input type="checkbox" name="homeDelivery"
                       value="true" />
            </td>
          </tr>
            <td>
              <i> Please tick ONLY if you plan to provide home delivery </i>
            </td>
          </tr>
          <tr>
            <td>Address Line One *</td>
            <td><input type="text" name="addressLineOne"
                       placeholder="Please Enter a value for this field"  
                       required/>
            </td>
          </tr>
          <tr>
            <td>Address Line Two </td>
            <td><input type="text" name="addressLineTwo" 
                       placeholder="This is optional" />
            </td>
          </tr>
          <tr>
            <td>City *</td>
            <td><input type="text" name="city" 
                       placeholder="Please Enter a value for this field" 
                       required/>
            </td>
           </tr>
           <tr>
             <td>State *</td>
             <td><input type="text" name="state" 
                        placeholder="Please Enter a value for this field" 
                        required/>
             </td>
           </tr>
           <tr>
             <td>Pin Code *</td>
             <td><input type="integer" name="pincode" 
                        placeholder="Please Enter a value for this field" 
                        required/>
             </td>
           </tr>
         </table>
         <br>
         <i>Fields marked with * are required fields</i>
         <br>
         <input type="reset" value="Reset" />
         <input type="submit" value="register" name="submit"/>
       </form>
     </div>
   </body>
</html>
