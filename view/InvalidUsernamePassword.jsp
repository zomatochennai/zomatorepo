<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>User Data</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <h1>Registration Form</h1>
    <div class="ex">
      <table style="with: 50%">
        <tr>
          <td>Invalid Username and/or Password</td>
        </tr>
        <tr>
          <td>Please login <a href = "LoginUser.jsp">here</a>.</td>
        </tr>
        <tr>
          <td>
            New User? Please register 
              <a href = "RegisterUser.jsp">here</a>.
          </td>
        </tr>
        <tr>
          <td>Forgot Password? 
            <a href = "ForgotPassword.jsp">Reset Password</a>.
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>
