<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Manage Tracking Page</title>
  </head>
  <style>
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
    <script type = "text/javascript">
      function togglediv(id) {
        var div = document.getElementById(id);
        div.style.display = div.style.display == "none" ? "block" : "none";
      }
    </script>
    <h1>Manage and update tracking for all of your Orders in one Page!</h1>
      <form action="http://localhost:8080/zomato/user" method="post">
        <table style="with: 5%" align="right">
          <tr>
            <td>
              <input type="submit" value="logout" name="submit" />
            </td>
          </tr>
        </table>
      </form>
    <div class="ex">
      <form action="http://localhost:8080/zomato/restaurant" method="get">
        <table style="with: 50%">
          <h2>Please Click a button to do any operation:</h2>
            <br>
            <tr>
              <th>Tracking ID</th>
              <th>Tracking Order ID</th>            
            </tr>
            <br>
            <input type="hidden" value="view/Managetracking.jsp" 
                   name="targetPage" />
            <c:forEach var="tracking" items="${trackings}">
            <tr>
              <td><c:out value="${tracking.id}" /></td>
              <td><c:out value="${tracking.order.id}" /></td>
              <td> 
                <input id="edittracking" type="button" value="update tracking"
                       onclick="togglediv('myedittrackingdiv')">
                <div id="myedittrackingdiv" style="display: none;">
                  <br>
                  <p>
                    Update Dispatched Status:
                    <input type="checkbox" value="true" 
                           id="new dispatched status" 
                           name="new dispatched status">
                    </input>
            <input type="submit" value="display trackings" name="submit" />
            <input type="hidden" value="view/Managetracking.jsp" 
                   name="targetPage" />
                  </p>
                  <br>
                  <p>
                    Update Delivered Status:
                    <input type="checkbox" value="true" 
                           id="new delivered status" 
                           name="new delivered status">
                    </input>
            <input type="submit" value="display trackings" name="submit" />
            <input type="hidden" value="view/Managetracking.jsp" 
                   name="targetPage" />
                  </p>
                  <br>
                  <p>
                    Update Estimate Delivery Date and Time:
                    <input type="text" value="new estimate delivery after edit" 
                           id="new estimate" name="new estimate">
                    </input>
            <input type="submit" value="display trackings" name="submit" />
            <input type="hidden" value="view/Managetracking.jsp" 
                   name="targetPage" />
                  </p>
                  <br>
                    <input type="submit" value="confirm update tracking" 
                           name="submit" />
                </div>
              </td>
            </tr>
            </c:forEach>
          </table>
        </div>
    </body>
</html>
