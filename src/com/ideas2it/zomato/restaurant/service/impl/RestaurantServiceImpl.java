package com.ideas2it.zomato.restaurant.service.impl;

import java.util.Set;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;
import com.ideas2it.zomato.review.Review;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.review.service.ReviewService;
import com.ideas2it.zomato.review.service.impl.ReviewServiceImpl;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.restaurant.service.RestaurantService;
import com.ideas2it.zomato.restaurant.dao.RestaurantDao;
import com.ideas2it.zomato.restaurant.dao.impl.RestaurantDaoImpl;

/**
 * Service class implementation for restaurant.
 */
public class RestaurantServiceImpl implements RestaurantService {

    RestaurantDao restaurantDao = new RestaurantDaoImpl();
    UserService userService = new UserServiceImpl();

    /**
     * Registers a new restaurant.
     *
     * @param restaurant
     *            - restaurant object about to be registered .
     *
     * @param address
     *            - address of the restaurant.
     *
     * @param userId
     *            - id of restaurant's vendor.
     *
     * @return true or false based on restaurant registration.
     */
    public boolean registerRestaurant(Restaurant restaurant, Address address,
                                                          int userId) {
        boolean isSuccesful = false;
        User user = userService.getUserById(userId);
        if(null == user.getRestaurant()) {
            restaurant.setUser(user);
            user.setRestaurant(restaurant);
            restaurant.setAddress(address);
            address.setRestaurant(restaurant);
            isSuccesful = restaurantDao.insertRestaurant(restaurant);
        }
        return isSuccesful;
    }
    
    /**
     * Returns all the restaurants in the database.
     *
     * @param classificationId
     *            - classification Id of restaurants to be fetched.
     *
     * @return set of restaurants.
     */
    public Set<Restaurant> getRestaurants(int classificationId) {
        return restaurantDao.getRestaurants(classificationId);
    }
    
    /**
     * Assigns a new classification.
     *
     * @param classification
     *            - classification of the restaurant .
     *
     * @param restaurant
     *            - restaurant object to be updated .
     */
    public void assignClassification(Classification classification,
                                        Restaurant restaurant) {
        Set<Classification> classifications = restaurant.getClassifications();
        classifications.add(classification);
        restaurant.setClassifications(classifications);
        restaurantDao.updateRestaurant(restaurant);
    }
    
    /**
     * Deletes the restaurant.
     *
     * @param id
     *            - id of restaurant about to be deleted .
     */
    public void deleteRestaurant(int id) {
        restaurantDao.deleteRestaurant(id);
    }
    
    /**
     * Adds a new classification.
     *
     * @param user
     *            - user object about to be added .
     *
     * @param restaurant
     *            - restaurant object about to be added .
     *
     * @param review
     *            - review object about to be added .
     */
    public void addReview(User user, Restaurant restaurant, Review review) {
        ReviewService reviewService = new ReviewServiceImpl(); 
        review.setRestaurant(restaurant);
        review.setUser(user);
        reviewService.add(review);
    }
    
    /**
     * Returns the restaurant specified by the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     *@return Restaurant for the specified id
     */
    public Restaurant getRestaurant(int id) {
        return restaurantDao.getRestaurantById(id);
    }
}
