package com.ideas2it.zomato.restaurant.dao;

import java.util.Set;

import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.classification.Classification;

/**
 * Interface implementation for Restaurant dao class.
 */
public interface RestaurantDao {

    /**
     * Inserts a new restaurant into the database.
     *
     * @param restaurant
     *            - restaurant object about to be inserted.
     *
     * @return true or false based on restaurant registration.
     */
    boolean insertRestaurant(Restaurant restaurant);
    
    /**
     * Updates the restaurant from the database.
     *
     * @param restaurant
     *            - restaurant object about to be updated.
     */
    void updateRestaurant(Restaurant restaurant);
    
    /**
     * Gets the restaurant from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return restaurant specified by the id.
     */
    Restaurant getRestaurantById(int id);
    
    /**
     * Deletes the restaurant from the database.
     *
     * @param id
     *            - id of restaurant to be deleted.
     */
    void deleteRestaurant(int id);
    
    /**
     * Gets the restaurant from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return list of restaurants under specified classification.
     */
    Set<Restaurant> getRestaurants(int classificationId);
}
