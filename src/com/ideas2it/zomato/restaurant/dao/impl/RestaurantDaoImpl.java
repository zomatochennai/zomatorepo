package com.ideas2it.zomato.restaurant.dao.impl;

import java.util.Set;
import java.util.HashSet;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.restaurant.dao.RestaurantDao;
import com.ideas2it.zomato.common.Config;

/**
 * Performs all the database related funcionalities for food.
 */
public class RestaurantDaoImpl implements RestaurantDao {

    /**
     * Inserts a new restaurant into the database.
     *
     * @param restaurant
     *            - restaurant object about to be inserted.
     *
     * @return true or false based on restaurant registration.
     */
    public boolean insertRestaurant(Restaurant restaurant) 
                throws HibernateException {
        boolean isSuccesful = false;
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.save(restaurant); 
           transaction.commit();
           isSuccesful = true;
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            isSuccesful = false;
            throw exception;
        } finally {
             session.close();
        }
        return isSuccesful;
    }
    
    /**
     * Updates the restaurant from the database.
     *
     * @param restaurant
     *            - restaurant object about to be updated.
     */
    public void updateRestaurant(Restaurant restaurant) 
                throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.update(restaurant); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Gets the restaurant from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return restaurant specified by the id.
     */
    public Restaurant getRestaurantById(int id) throws HibernateException {
        Restaurant restaurant = new Restaurant();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           Criteria criteria = session.createCriteria(Restaurant.class);
           criteria.add(Restrictions.eq("id", id));
           restaurant = (Restaurant)criteria.uniqueResult();
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
        return restaurant;
    }
    
    /**
     * Deletes the restaurant from the database.
     *
     * @param id
     *            - id of restaurant to be deleted.
     */
    public void deleteRestaurant(int id) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           Restaurant restaurant = (Restaurant) session.get(Restaurant.class, id);
           session.delete(restaurant);
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Returns all the restaurants in the database.
     *
     * @param classificationId
     *            - classification Id of the restaurants.
     *
     * @return set of restaurants.
     */
    public Set<Restaurant> getRestaurants(int classificationId) 
                           throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        Set<Restaurant> restaurants = new HashSet<>();
        try {
           transaction = session.beginTransaction();
           restaurants = new HashSet<>(session.createCriteria(Restaurant.class)
                              .createCriteria("classifications")
                              .add( Restrictions
                              .eq("id", classificationId)).list());
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
        return restaurants;
    }
}
