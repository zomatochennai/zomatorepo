package com.ideas2it.zomato.category.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.category.dao.CategoryDao;
import com.ideas2it.zomato.common.Config;
import com.ideas2it.zomato.restaurant.Restaurant;

/**
 * Performs all the database related funcionalities for category object.
 */
public class CategoryDaoImpl implements CategoryDao {

    private static final Logger logger = Logger.getLogger(CategoryDaoImpl.class);

    /**
     * Inserts a new address into the database.
     *
     * @param category
     *            - category object about to be inserted.
     */
    public void insert(Category category) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.save(category); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Gets the categories for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     */
    public Set<Category> getCategories(int restaurantId) 
                         throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        Set<Category> categories = new HashSet<>();
        try {
           transaction = session.beginTransaction();
           logger.debug("rest id " + restaurantId);
           Criteria criteria = session.createCriteria(Category.class)
                                      .createCriteria("menuItems")
                                      .createCriteria("restaurant");
           criteria.add(Restrictions.eq("id", restaurantId));
           categories = new HashSet<>(criteria.list());
           for (Category category : categories) {
               logger.debug(category.getName());
           }
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
        return categories;
    }
    
    /**
     * Get all the categories from the database.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     */
    public Set<Category> getAllCategories() {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        Set<Category> categories = new HashSet<>();
        try {
           transaction = session.beginTransaction();
           categories = new HashSet(session.createQuery("FROM Category").list());
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
        return categories;
    }
    
    /**
     * Deletes the category from the database specified by id.
     *
     * @param id
     *            - id of category to be deleted.
     */
    public void delete(int id) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           Category category = (Category)session.get(Category.class, id);
           session.delete(category);
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
}
