package com.ideas2it.zomato.order.controller;

import java.lang.NullPointerException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.order.service.OrderService;
import com.ideas2it.zomato.order.service.impl.OrderServiceImpl;
import com.ideas2it.zomato.tracking.Tracking;
import com.ideas2it.zomato.tracking.service.TrackingService;
import com.ideas2it.zomato.tracking.service.impl.TrackingServiceImpl;

/**
 * Controller implementation for order servlet.
 */
public class OrderController {

    OrderService orderService = new OrderServiceImpl();
    TrackingService trackingService = new TrackingServiceImpl();
    
    private static final Logger logger = Logger.getLogger(OrderController.class);
    
    /**
     * Returns the order and details for the user.
     *
     * @param userId
     *            - user id for the required order .
     *
     * @return Order of the user.
     */
    public Order getOrder(int userId) {
        Order order = new Order();
        try {
            order = orderService.getOrder(userId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return order;
    }
    
    /**
     * Returns the tracking for the order.
     *
     * @param orderId
     *            - order id for the required tracking .
     *
     * @return tracking of the order.
     */
    public Tracking getTracking(int orderId) {
        Tracking tracking = new Tracking();
        try {
            tracking = trackingService.getTracking(orderId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return tracking;
    }
    
    /**
     * Creates a new order for the payment.
     *
     * @param userId
     *            - user id for the required order .
     *
     * @param paymentId
     *            - payment id for the required order .
     */
    public void createOrder(int userId, int paymentId) {
        try {
            orderService.createOrder(userId, paymentId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
}
