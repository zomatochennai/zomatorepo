package com.ideas2it.zomato.exception;

public class NewException extends Exception {

    public NewException(String message) {
        super(message);
    }
}
