package com.ideas2it.zomato.role.controller;

import java.lang.NullPointerException;
import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.role.service.RoleService;
import com.ideas2it.zomato.role.service.impl.RoleServiceImpl;

/**
 * Controller implementation for role servlet.
 */
public class RoleController {

    RoleService roleService = new RoleServiceImpl();
    private static final Logger logger = Logger.getLogger(RoleController.class);

    /**
     * Adds a new role.
     *
     * @param role
     *            - role object to be added .
     */
    public void add(Role role) {
        try {
            roleService.add(role);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Deletes the role specified by id.
     *
     * @param roleId
     *            - id of role to be deleted .
     */
    public void remove(int roleId) {
        try {
            roleService.delete(roleId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
    }
    
    /**
     * Returns all the user role from database.
     *
     * @return set of roles.
     */
    public Set<Role> getRoles() {
        Set<Role> roles = new HashSet<>();
        try {
            roles = roleService.getRoles();
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return roles;
    }
}
