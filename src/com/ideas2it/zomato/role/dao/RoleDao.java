package com.ideas2it.zomato.role.dao;

import java.util.Set;

import com.ideas2it.zomato.role.Role;

/**
 * Interface implementation for Role dao class
 */
public interface RoleDao {

    /**
     * Inserts a new role into the database.
     *
     * @param role
     *            - role object about to be inserted.
     */
    void insert(Role role);
    
    /**
     * Deletes the role from the database specified by id.
     *
     * @param id
     *            - id of role to be deleted.
     */
    void delete(int id);
    
    /**
     * Returns the role specified by id.
     *
     * @param id
     *            - id of the role .
     *
     * @return role specified by id.
     */
    Role getRole(int id);
    
   /**
     * Returns all roles in database.
     *
     * @return user roles.
     */
    Set<Role> getRoles();
}
