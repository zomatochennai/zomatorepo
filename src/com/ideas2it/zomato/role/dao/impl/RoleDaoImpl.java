package com.ideas2it.zomato.role.dao.impl;

import java.util.Set;
import java.util.HashSet;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ideas2it.zomato.common.Config;
import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.role.dao.RoleDao;

/**
 * Performs all the database related funcionalities for role.
 */
public class RoleDaoImpl implements RoleDao {

    /**
     * Inserts a new role into the database.
     *
     * @param role
     *            - role object about to be inserted.
     */
    public void insert(Role role) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.save(role); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Deletes the role from the database specified by id.
     *
     * @param id
     *            - id of role to be deleted.
     */
    public void delete(int id) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Role role = (Role) session.get(Role.class, id);
            session.delete(role);
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) { 
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
    }
    
    /**
     * Returns all roles in database.
     *
     * @return user roles.
     */
    public Set<Role> getRoles() throws HibernateException {
        int adminId = 1; 
        Set<Role> roles = new HashSet<>();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Role.class);
            criteria.add(Restrictions.ne("id", adminId));
            roles = new HashSet<>(criteria.list());
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
        return roles;
    }

    /**
     * Returns the role specified by id.
     *
     * @param id
     *            - id of the role .
     *
     * @return role specified by id.
     */
    public Role getRole(int id) throws HibernateException {
        Role role = new Role();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Role.class);
            criteria.add(Restrictions.eq("id", id));
            role = (Role)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
            session.close(); 
        }
        return role;
    }
}
