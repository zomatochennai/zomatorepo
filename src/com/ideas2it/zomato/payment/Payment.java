package com.ideas2it.zomato.payment;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.order.Order;

/**
 * This is model class for Cart .
 */
public class Payment {

    private int id;
    private User user;
    private float cost;
    private String time;
    private long cardNumber;
    private String expiryDate;
    private String customerName;
    private Order order;
    
    /*
     * Setters and Getters.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setTime(String time) {
        this.time = time;
    }
    
    public String getTime() {
        return time;
    }
    
    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public long getCardNumber() {
        return cardNumber;
    }
    
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public String getExpiryDate() {
        return expiryDate;
    }
    
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    
    public String getCustomerName() {
        return customerName;
    }
    
    public void setCost(float cost) {
        this.cost = cost;
    }
    
    public float getCost() {
        return cost;
    }
    
    public void setOrder(Order order) {
        this.order = order;
    }
    
    public Order getOrder() {
        return order;
    }
}
