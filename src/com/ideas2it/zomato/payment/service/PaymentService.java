package com.ideas2it.zomato.payment.service;

import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.payment.dao.PaymentDao;

/*
 * This interface handles access between Payment DAO and controller layer 
 */
public interface PaymentService {

   /**
     * Creates a new payment for the cart.
     *
     * @param userId
     *            - user id of the payment.
     */
    Payment createPayment(int userId);
    
    /**
     * Returns the payment for the id.
     *
     * @param id
     *            - id of the payment.
     */
    Payment getPayment(int id);
}
