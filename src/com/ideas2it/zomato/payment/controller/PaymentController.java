package com.ideas2it.zomato.payment.controller;

import java.lang.NullPointerException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;

import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.payment.service.PaymentService;
import com.ideas2it.zomato.payment.service.impl.PaymentServiceImpl;

/**
 * Controller implementation for payment servlet.
 */
public class PaymentController {

    PaymentService paymentService = new PaymentServiceImpl();
    private static final Logger logger = Logger.getLogger(PaymentController.class); 
    
    /**
     * Creates a new payment for the cart.
     *
     * @param userId
     *            - user id for the payment.
     */
    public Payment createPayment(int userId) {
        Payment payment = new Payment();
        try {
            payment = paymentService.createPayment(userId);
        } catch (NullPointerException exception) {
            logger.error("Null pointer exception" + exception);
        } catch (HibernateException exception) {
            logger.error("Hibenate exception" + exception);
        } catch (Exception exception) {
            logger.error("Exception" + exception);
        }
        return payment;
    }


}
