package com.ideas2it.zomato.classification.dao;

import java.util.Set;

import com.ideas2it.zomato.classification.Classification;

/**
 * Interface implementation for Classification dao class
 */
public interface ClassificationDao {

    /**
     * Inserts a new classification into the database.
     *
     * @param classification
     *            - classification object to be inserted.
     */
    void insert(Classification classification);
    
    /**
     * Returns all the classifications from the database.
     *
     * @return set of classifications.
     */
    Set<Classification> getClassifications();
    
    /**
     * Deletes the classification from the database specified by id.
     *
     * @param id
     *            - id of classification to be deleted.
     */
    void deleteClassification(int id);
}
