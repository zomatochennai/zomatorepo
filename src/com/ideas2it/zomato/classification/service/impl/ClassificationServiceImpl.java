package com.ideas2it.zomato.classification.service.impl;

import java.util.Set;

import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.classification.service.ClassificationService;
import com.ideas2it.zomato.classification.dao.ClassificationDao;
import com.ideas2it.zomato.classification.dao.impl.ClassificationDaoImpl;

/**
 * Service class implementation for classification.
 */
public class ClassificationServiceImpl implements ClassificationService {

    ClassificationDao classificationDao = new ClassificationDaoImpl();

    /**
     * Adds a new classification.
     *
     * @param classification
     *            - classification object about to be added .
     */
    public void add(Classification classification) {
        classificationDao.insert(classification);
    }
    
    /**
     * Removes the classification .
     *
     * @param id
     *            - id of classification .
     */
    public void remove(int id) {
        classificationDao.deleteClassification(id);
    }
    
    /**
     * Returns all the classifications in the database.
     *
     * @return set of classifications.
     */
    public Set<Classification> getClassifications() {
        return classificationDao.getClassifications();
    }
}
