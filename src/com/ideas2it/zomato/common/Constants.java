package com.ideas2it.zomato.common;

/**
 * Contains constant variables required for the project.
 */
public class Constants {
    
    //JSP pages
    public static final String CLASSIFICATION_LIST = "view/Classification"
                                                       +"List.jsp";
    public static final String MENUITEM_LIST = "view/FoodItemList.jsp";
    public static final String FORGOT_PASSWORD = "view/ForgotPassword.jsp";
    public static final String INVALID_EMAIL_PASSWORD = "view/InvalidUsername"
                                                         +"Password.jsp";
    public static final String LOGIN_USER = "view/LoginUser.jsp";
    public static final String REGISTER_USER = "view/RegisterUser.jsp";
    public static final String REGISTER_SUCCESFUL = "view/Registration"
                                                     +"Successful.jsp";
    public static final String RESTAURANT = "view/Restaurant.jsp";
    public static final String RESTAURANT_LIST = "view/RestaurantList.jsp";
    public static final String WELCOME = "http://localhost:8080/zomato/";
    public static final String RESTAURANT_HOME = "view/RestaurantHome.jsp";
    public static final String ADMIN_HOME = "view/AdminHome.jsp";
    public static final String CUSTOMER_HOME = "view/CustomerHome.jsp";
    public static final String ERROR = "view/SomethingWentWrong.jsp";
    
}
