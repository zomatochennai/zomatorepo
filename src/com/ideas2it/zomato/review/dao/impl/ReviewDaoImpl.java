package com.ideas2it.zomato.review.dao.impl;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ideas2it.zomato.review.Review;
import com.ideas2it.zomato.review.dao.ReviewDao;
import com.ideas2it.zomato.common.Config;

/**
 * Service class implementation for review .
 */
public class ReviewDaoImpl implements ReviewDao {

    /**
     * Inserts a new review into the database.
     *
     * @param review
     *            - review object to be inserted.
     */
    public void insert(Review review) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.save(review); 
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Deletes the review from the database.
     *
     * @param id
     *            - id of review to be deleted.
     */
    public void delete(int id) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           Review review = (Review) session.get(Review.class, id);
           session.delete(review);
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }

}
