package com.ideas2it.zomato.cartDetail;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.menuItem.MenuItem;

/**
 * This is model class for CartDetail .
 */
public class CartDetail {
    private int id;
    private Cart cart;
    private MenuItem menuItem;
    private int quantity;
    private float price;
    
    public CartDetail(MenuItem menuItem) {
        this.menuItem = menuItem;
    }
    
    /*
     * Setters and Getters.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    } 
    
    public void setCart(Cart cart) {
        this.cart = cart;
    }
    
    public Cart getCart() {
        return cart;
    }
    
    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }
    
    public MenuItem getMenuItem() {
        return menuItem;
    } 
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public void setPrice(float price) {
        this.price = price;
    }
    
    public float getPrice() {
        return price;
    }
}
