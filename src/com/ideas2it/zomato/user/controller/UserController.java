package com.ideas2it.zomato.user.controller;

import org.apache.log4j.Logger;

import com.ideas2it.zomato.exception.NewException;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;

/**
 * Controller implementation for user servlet.
 */
public class UserController {

    UserService userService = new UserServiceImpl();
    private static final Logger logger = Logger.getLogger(UserController.class);
    
    /**
     * <p>
     * Registers a new user to the application, along with a address and role.
     * </p>
     *
     * @param user
     *            - user object about to be added .
     *
     * @param address
     *            - address of the user to be added.
     *
     * @return true or false based on user group insertion.
     */
    public boolean register(User user, Address address, int roleId) {
        boolean value = false;
        try {
            value = userService.register(user, address, roleId);
        } catch (NewException e) {
            logger.error("Error occured while registering user "
                         + user.getFirstName() + e);
        }
        return value;
    }
    
    /**
     * <p>
     * Adds a new address to the user.
     * </p>
     *
     * @param user
     *            - user object about to be added .
     *
     * @param address
     *            - address of the user to be added.
     *
     * @return true or false based on address assigning.
     */
    public boolean addAddress(User user, Address address) {
        boolean value = false;
        try {
            value = userService.addAddress(user, address);
        } catch (NewException e) {
            logger.error("Error occured while adding new address to user "
                         + user.getFirstName() + e);
        }
        return value;
    }
    
    /**
     * <p>
     * Logs in the user to the application.
     * User is redirected to the next page based on the user's role.
     * If the entered email does not exist in the database or if the email 
     * validation fails or if the entered password does not match the email
     * login fails. 
     * </p>
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user.
     *
     * @return user that is logged in.
     */
    public User login(String email, String password) {
        User user = new User();
        try {
            user = userService.login(email, password);
        } catch (NewException e) {
            logger.error("Error occured while logging in user with email " 
                         + email + e);
        }
        return user;
    }
    
    /**
     * <p>
     * Logs out the user that is already logged into the application.
     * </p>
     *
     * @param user
     *            - user to be logged out.
     *
     * @return true or false based on user group insertion.
     */
    public boolean logout(User user) {
        boolean value = false;
        try {
            value = userService.logout(user.getId());
        } catch (NewException e) {
            logger.error("Error occured while logging out user "
                         + user.getFirstName() + e);
        }
        return value;    
    }
    
    /**
     * <p>
     * Enables user to reset old password in case the user is not able to recall
     * their old password.
     * Prompts the user to enter the registered email along with two security 
     * questions which was answered by the user at the time of registration.
     * </p>
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user.
     *
     * @param petName
     *            - pet name of the user.
     *
     * @param bicycleName
     *            - bicycle name of the user .
     *
     * @return true or false based on user group insertion.
     */
    public boolean resetPassword(String email, String password, String petName, 
                              String bicycleName) {
        boolean value = false;
        try {
            value = userService.resetPassword(email, password, petName, bicycleName);
        } catch (NewException e) {
            logger.error("Error occured while resetting password for user"
                         + " with email " + email + e);
        }
        return value;
    }
    
    /**
     * <p>
     * Gives admin access to a user. Only a admin user can give admin access 
     * to other users.
     * </p>
     *
     * @param adminUser
     *            - adminUser who grants admin access .
     *
     * @param user
     *            - user who is assigned as admin.
     *
     * @return true or false based on giving admin access.
     */
    public boolean giveAdminAccess(User adminUser, User user) {
        boolean value = false;
        try {
            value = userService.giveAdminAccess(adminUser, user);
        } catch (NewException e) {
             logger.error("Error occured while giving admin access to user" 
                          + user.getFirstName() + e);
        }
        return value;
    }
}
