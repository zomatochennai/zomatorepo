package com.ideas2it.zomato.user.dao;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.exception.NewException;
import com.ideas2it.zomato.address.Address;

/**
 * Interface implementation for User dao class
 */
public interface UserDao {

    /**
     * <p>
     * Inserts a new user into the database.
     * </p>
     *
     * @param user
     *            - user object about to be inserted
     *
     * @return true or false based on user group deletion
     */
    boolean insert(User user) throws NewException;
    
    /**
     * <p>
     * Updates the user in the database.
     * </p>
     *
     * @param user
     *            - user object to be updated.
     */
    void update(User user) throws NewException;
    
    /**
     * <p>
     * Gets a user from database based on email.
     * </p>
     *
     * @param email
     *            - email of the user.
     *
     * @return user specified by email.
     */
    User getUserByEmail(String email) throws NewException; 
    
    /**
     * <p>
     * Gets a user from database based on id.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @return user specified by id.
     */
    User getUserById(int id);
    
    /**
     * <p>
     * Deletes the user from the database specified by id.
     * </p>
     *
     * @param id
     *            - id of user to be deleted.
     */
    void delete(int id) throws NewException;
    
    /**
     * <p>
     * Returns the user if he has admin rights.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @return admin user specified by id.
     */
    User returnIfAdmin(int id) throws NewException;
}
