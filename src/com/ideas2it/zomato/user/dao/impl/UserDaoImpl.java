package com.ideas2it.zomato.user.dao.impl;

import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ideas2it.zomato.exception.NewException;
import com.ideas2it.zomato.common.Config;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.dao.UserDao;

/**
 * Performs insert, update, delete, and read funcionalities for user.
 */
public class UserDaoImpl implements UserDao {

    private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

    /**
     * <p>
     * Inserts a new user into the database.
     * </p>
     *
     * @param user
     *            - user object about to be inserted.
     *
     * @return    - true for succesful insertion of user.
     *            - false for unsuccesful insertion of user.
     */
    public boolean insert(User user) throws NewException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        boolean success = false;
        try {
           transaction = session.beginTransaction();
           session.save(user);
           transaction.commit();
           success = true;
        } catch (HibernateException e) {
            if (null != transaction) {
                transaction.rollback();
                success = false;
            }
            throw new NewException("Error occured while inserting user "
                                   + user.getFirstName() + e);
        } finally {
             session.close();
        }
        return success;
    }
    
    /**
     * <p>
     * Updates the user in the database.
     * </p>
     *
     * @param user
     *            - user object to be updated.
     */
    public void update(User user) throws NewException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.update(user); 
           transaction.commit();
        } catch (HibernateException e) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw new NewException("Error occured while updating user "
                                   + user.getFirstName() + e);
        } finally {
             session.close();
        }
    }
    
    /**
     * <p>
     * Gets a user from database based on email.
     * </p>
     *
     * @param email
     *            - email of the user.
     *
     * @return user specified by email. Returns empty user if email
     *         does not exist.
     */
    public User getUserByEmail(String email) throws NewException {
        User user = new User();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("email", email));
            user = (User)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw new NewException("Error occured while inserting user "
                                   + user.getEmail() + e);
        } finally {
            session.close(); 
        }
        return user;
    }
    
    /**
     * <p>
     * Gets a user from database based on id.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @return user specified by id. Returns empty user if id does not exist.
     */
    public User getUserById(int id) {
        User user = new User();
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("id", id));
            user = (User)criteria.uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (null != transaction) {
                transaction.rollback();
            } 
            logger.error("Error occured while getting user with id "
                                   + id + e);
        } finally {
            session.close(); 
        }
        return user;
    }
    
    /**
     * <p>
     * Deletes the user from the database specified by id.
     * </p>
     *
     * @param id
     *            - id of user to be deleted.
     */
    public void delete(int id) throws NewException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            User user = (User) session.get(User.class, id);
            session.delete(user);
            transaction.commit();
        } catch (HibernateException e) {
            if (null != transaction) { 
                transaction.rollback();
            } 
            throw new NewException("Error occured while deleting user with id "
                                   + id + e);
        } finally {
            session.close(); 
        }
    }
    
    /**
     * <p>
     * Returns the user if he has admin rights.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @return admin user specified by id. Returns empty user if id
     *         specified does not belong to a admin user.
     */
    public User returnIfAdmin(int id) throws NewException {
        User user = new User();
        int adminRoleId = 1;
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            user = (User)session.createCriteria(User.class)
                            .add( Restrictions.eq("id", id))
                            .createCriteria("role")
                            .add( Restrictions.eq("id", adminRoleId))
                            .uniqueResult();
            transaction.commit();
        } catch (HibernateException e) {
            if (null != transaction) {
                transaction.rollback();
            }
            throw new NewException("Error occured while getting admin user "
                                   + "with id " + id + e);
        } finally {
            session.close(); 
        }
        return user;
    }
}
