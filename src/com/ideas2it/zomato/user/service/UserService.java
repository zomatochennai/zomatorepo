package com.ideas2it.zomato.user.service;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.exception.NewException;
import com.ideas2it.zomato.address.Address;

/**
 * Interface implementation for user service class.
 */
public interface UserService {

    /**
     * <p>
     * Registers a new user to the application, along with a address and role.
     * </p>
     *
     * @param user
     *            - user object about to be added .
     *
     * @param address
     *            - address of the user to be added.
     *
     * @param roleId
     *            - id of the user role.
     *
     * @return true or false based on user registration.
     */
    boolean register(User user, Address address, int roleId)throws NewException;
    
    /**
     * <p>
     * Adds a new address to the user.
     * </p>
     *
     * @param user
     *            - user object about to be added .
     *
     * @param address
     *            - address of the user to be added.
     *
     * @return true or false based on address assigning.
     */
    boolean addAddress(User user, Address address) throws NewException;
    
    /**
     * <p>
     * Logs in the user to the application.
     * If the entered email does not exist in the database or if the entered
     * password does not match the email, login fails.
     * </p>
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user .
     */
    User login(String email, String password) throws NewException;
    
    /**
     * <p>
     * Logs out the user from the application based on id.
     * </p>
     *
     * @param userId
     *            - id of the user.
     *
     * @return true or false based on user logout.
     */
    boolean logout(int id) throws NewException;
    
    /**
     * <p>
     * Enables user to reset old password after checking the security questions
     * petName and bicycleName.
     * </p>
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user.
     *
     * @param petName
     *            - pet name of the user.
     *
     * @param bicycleName
     *            - bicycle name of the user .
     *
     * @return true or false based on password reset.
     */
    boolean resetPassword(String email, String password, String petName, 
                     String bicycleName) throws NewException;
    
    /**
     * <p>
     * Gives admin access to a user.
     * </p>
     *
     * @param adminUser
     *            - adminUser who grants admin access .
     *
     * @param user
     *            - user who is assigned as admin.
     *
     * @return true or false based on giving admin access.
     */
    boolean giveAdminAccess(User adminUser, User user) throws NewException;
    
    /**
     * <p>
     * Returns the user specified by the id.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @param user
     *            - user for the specified id .
     */
    User getUserById(int id);
    
    /**
     * <p>
     * Returns the user specified by the id.
     * </p>
     *
     * @param id
     *            - id of the user.
     */
    void updateUser(int id) throws NewException;
}
