package com.ideas2it.zomato.cart.dao;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cartDetail.CartDetail;

/*
 * This interface handles access between Cart Service and hibernate .
 */
public interface CartDao {
 
    /**
     * Returns the cart for the specified user.
     *
     * @param userId
     *            - id of the user.
     *
     * @return cart object for the id.
     */
    Cart getCart(int userId);
    
    /**
     * Updates the cart into the database.
     *
     * @param cart
     *            - cart to be updated.
     */
    void update(Cart cart);
    
    /**
     * Deletes the cart detail from the database specified by id.
     *
     * @param id
     *            - id of user to be deleted.
     *
     * @return true or false based on cart detail removal.
     */
    boolean removeCartDetail(int id);
}
