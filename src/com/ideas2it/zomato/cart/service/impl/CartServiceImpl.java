package com.ideas2it.zomato.cart.service.impl;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cartDetail.CartDetail;
import com.ideas2it.zomato.cart.dao.CartDao;
import com.ideas2it.zomato.cart.dao.impl.CartDaoImpl;
import com.ideas2it.zomato.cart.service.CartService;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;
import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.menuItem.service.MenuItemService;
import com.ideas2it.zomato.menuItem.service.impl.MenuItemServiceImpl;

/*
 * This class handles creation and updation of shopping cart.
 */
public class CartServiceImpl implements CartService {

    CartDao cartDao = new CartDaoImpl();
    MenuItemService menuItemService = new MenuItemServiceImpl();
    UserService userService = new UserServiceImpl();

    /**
     * User adds a new item to the cart.
     *
     * @param menuItemId
     *            - id of the chosen menu item.
     *
     * @param userId
     *            - id of cart's user.
     */
    public void addToCart(int menuItemId, int userId) {
        MenuItem menuItem = menuItemService.getMenuItem(menuItemId);
        User user = userService.getUserById(userId);
        CartDetail cartDetail = new CartDetail(menuItem);
        cartDetail.setPrice(menuItem.getPrice());
        Cart cart = cartDao.getCart(userId);
        if (0 == cart.getId()) {
            cart = new Cart(user);
        }
        cart.addCartDetail(cartDetail);
        cartDao.update(cart);
    }
    
    /**
     * Returns the cart for the specified user.
     *
     * @param userId
     *            - id of the user.
     *
     * @return cart object for the id.
     */
    public Cart getCart(int userId) {
        return cartDao.getCart(userId);
    }
    
    /**
     * Removes the cart detail specified, from the cart.
     *
     * @param cartDetailId
     *            - id of cartDetail.
     *
     * @return True or false based on cart detail deletion.
     */
    public boolean removeCartDetail(int cartDetailId) {
        return cartDao.removeCartDetail(cartDetailId);
    }
}
