package com.ideas2it.zomato.food.dao.impl;

import java.util.Set;
import java.util.HashSet;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.food.dao.FoodDao;
import com.ideas2it.zomato.common.Config;

/**
 * Performs insert, update, delete, and read funcionalities for food.
 */
public class FoodDaoImpl implements FoodDao {

    /**
     * Inserts a new food into the database.
     *
     * @param food
     *            - food object about to be inserted.
     */
    public void insert(Food food) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           session.save(food);
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Deletes the food from the database specified by id.
     *
     * @param id
     *            - id of food to be deleted.
     */
    public void delete(int id) throws HibernateException {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        try {
           transaction = session.beginTransaction();
           Food food = (Food)session.get(Food.class, id);
           session.delete(food);
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
    }
    
    /**
     * Gets all the foods from the database.
     *
     * @return all foods in the database.
     */
    public Set<Food> getFoods() {
        Session session = Config.factory.openSession();
        Transaction transaction = null;
        Set<Food> foods = new HashSet<>();
        try {
           transaction = session.beginTransaction();
           foods = new HashSet(session.createQuery("FROM Food").list());
           transaction.commit();
        } catch (HibernateException exception) {
            if (null != transaction) {
                transaction.rollback();
            } 
            throw exception;
        } finally {
             session.close();
        }
        return foods;
    }
}
