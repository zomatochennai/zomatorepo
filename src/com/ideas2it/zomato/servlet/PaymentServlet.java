package com.ideas2it.zomato.servlet;

import java.io.IOException;
import java.util.Set;
import java.util.HashSet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletException;

import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.payment.controller.PaymentController;
import com.ideas2it.zomato.common.Constants;

/**
 * Servlet implementation for Payment.
 */
public class PaymentServlet extends HttpServlet {

    PaymentController paymentController = new PaymentController();
    private static final String CHECKOUT = "checkout";
    
    /**
     * Inserts and updates tables into the database for payment.
     *
     * @param request
     *            - request object of HTTPservlet.
     *
     * @param response
     *            - response object of HTTPservlet.
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
			               throws ServletException, IOException {
	    response.setContentType("text/html");
	    String input = request.getParameter("submit");
	    HttpSession session = request.getSession();
	    
	    switch (input) {
            case CHECKOUT :
                createPayment(request, response, session);
                break;
        }
    }
    
    /**
     * Creates and displays payment for the selected menuitems.
     *
     * @param request
     *        - request object of HTTPservlet.
     *
     * @param response
     *        - response object of HTTPservlet.
     *
     * @param session
     *            - session object of HTTPservlet.
     */
    private void createPayment(HttpServletRequest request, 
                          HttpServletResponse response, HttpSession session) 
                          throws ServletException, IOException {
        int userId = (Integer)(session.getAttribute("userId"));
        Payment payment = paymentController.createPayment(userId);
        request.setAttribute("payment", payment);
        String targetPage = request.getParameter("targetPage");
        request.getRequestDispatcher(targetPage).forward(request, response);
    }
}
