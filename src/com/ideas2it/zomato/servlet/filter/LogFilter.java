package com.ideas2it.zomato.servlet.filter;

import java.io.IOException;
import java.util.Date;
import java.util.Enumeration;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

public class LogFilter implements Filter {

    private ServletContext context;
    
    public void init(FilterConfig fConfig) throws ServletException {
        this.context = fConfig.getServletContext();
        this.context.log("Log Filter initialized");
    }

    public void doFilter(ServletRequest request, ServletResponse response,
                       FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        Enumeration<String> params = req.getParameterNames();
        while(params.hasMoreElements()){
            String name = params.nextElement();
            String value = request.getParameter(name);
            this.context.log("IP: " + req.getRemoteAddr() 
                                + " ::Request Params::{"+name+"="+value+"} " 
                                + " Date/Time: " + new Date().toString());
        }
        
        Cookie[] cookies = req.getCookies();
        if(cookies != null){
            for(Cookie cookie : cookies){
                this.context.log("IP: " + req.getRemoteAddr() 
                                    + " ::Cookie::{"+cookie.getName()
                                    +","+cookie.getValue()+"} " 
                                    + " Date/Time: " + new Date().toString());
            }
        }
        chain.doFilter(request, response);
    }
    
    public void destroy() {
    
    }
}
