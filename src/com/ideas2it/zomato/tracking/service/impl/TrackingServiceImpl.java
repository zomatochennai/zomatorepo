package com.ideas2it.zomato.tracking.service.impl;

import com.ideas2it.zomato.tracking.Tracking;
import com.ideas2it.zomato.tracking.service.TrackingService;
import com.ideas2it.zomato.tracking.dao.TrackingDao;
import com.ideas2it.zomato.tracking.dao.impl.TrackingDaoImpl;

/*
 * This class handles access between Tracking option DAO and controller layer.
 */
public class TrackingServiceImpl implements TrackingService {

    TrackingDao trackingDao = new TrackingDaoImpl();

    /**
     * Returns the tracking for the order.
     *
     * @param orderId
     *            - order id for the required tracking .
     *
     * @return tracking of the order.
     */
    public Tracking getTracking(int orderId) {
        return trackingDao.getTracking(orderId);
    }
}
