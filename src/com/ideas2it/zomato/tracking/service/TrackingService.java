package com.ideas2it.zomato.tracking.service;

import com.ideas2it.zomato.tracking.Tracking;

/*
 * This interface handles access between Tracking DAO and controller layer .
 */
public interface TrackingService {

    /**
     * Returns the tracking for the order.
     *
     * @param orderId
     *            - order id for the required tracking .
     *
     * @return tracking of the order.
     */
    Tracking getTracking(int orderId);
}
